<?php

namespace Jackrobin\Emotion\Services;

use Illuminate\Support\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Jackrobin\Contracts\Emotion\Models\Emotion as EmotionContract;
use Jackrobin\Contracts\Emotion\Traits\Emotional as EmotionalContract;
use Jackrobin\Contracts\Emotion\Traits\Expressive as ExpressiveContract;
use Jackrobin\Contracts\Emotion\Services\EmotionService as EmotionServiceContract;
use Jackrobin\Contracts\Emotion\Models\Pivot\EmotionalExpression as EmotionalExpressionContract;

/**
 * Handles all emotion-related processes, e.g. adding and removing emotions.
 *
 * EmotionService can be invoked directly.
 *
 * @example
 * $service = app(EmotionServiceContract::class); // or new EmotionService()
 * $service->hasExpression($emotion, $emotion, $expressive);
 * $service->addExpression($emotional, 'like', $expressive);
 * $service->addExpression($book, 'read', $user);
 */
class EmotionService implements EmotionServiceContract
{
    /**
     * Removes all emotions from an entity.
     *
     * If $expressive is provided, only emotions by $expressive are removed. Otherwise, removes all emotions.
     *
     * @param EmotionalContract       $emotional  Entity to be removed from
     * @param ExpressiveContract|null $expressive Only removes emotions by this entity
     * @return int  Number of emotions removed
     * @example
     * $service->removeExpressions($emotional);              // all emotions are removed
     * $service->removeExpressions($emotional, $expressive); // only $user's emotions are removed
     * $service->removeExpressions($book, $user);
     */
    public function removeExpressions($emotional, $expressive = null): int
    {
        $query = $emotional->expressions();

        if (isset($expressive)) {
            $query->where(self::getExpressive($expressive));
        }

        return $query->delete();
    }

    /**
     * Extracts identifiers from expressive.
     *
     * @param ExpressiveContract $expressive
     * @return  array  In shape ['expressive_id' => int|string, 'expressive_type' => string]
     */
    public static function getExpressive($expressive): array
    {
        return is_object($expressive) ? [
            'expressive_id' => $expressive->getKey(),
            'expressive_type' => get_class($expressive),
        ] : [
            'expressive_id' => $expressive,
            'expressive_type' => config('emotion.default_models.expressive'),
        ];
    }

    /**
     * Adds or removes an emotion from an entity.
     *
     * @param EmotionalContract       $emotional  Entity to be removed from
     * @param EmotionContract         $emotion
     * @param ExpressiveContract|null $expressive Only removes emotions by this entity
     * @return  mixed  Success
     * @example
     * $service->toggleExpression($emotional, 'like', $user); // $emotional is now "liked"
     * $service->toggleExpression($emotional, 'like', $user); // $emotional is no longer "liked"
     * $service->toggleExpression($book, 'read', $user);
     */
    public function toggleExpression($emotional, $emotion, $expressive)
    {
        return $this->hasExpression($emotional, $emotion, $expressive) ?
            $this->removeExpression($emotional, $emotion, $expressive) :
            $this->addExpression($emotional, $emotion, $expressive);
    }

    /**
     * Checks if entity has an emotion.
     *
     * If $expressive is provided, looks for $emotion by $expressive.
     *
     * @param EmotionalContract       $emotional  Entity to be removed from
     * @param EmotionContract         $emotion
     * @param ExpressiveContract|null $expressive Only removes emotions by this entity
     * @return  int  Number of matching emotions
     * @example
     * $service->addExpression($emotional, 'like', $user);       // seed $emotional
     *
     * $service->hasExpression($emotional, 'like');              // => 1
     * $service->hasExpression($emotional, 'like', $user);       // => 1
     * $service->hasExpression($emotional, 'like', $other_user); // => 0
     * $service->hasExpression($book, 'read', $user);
     */
    public function hasExpression($emotional, $emotion, $expressive = null): int
    {
        $query = self::getExpressions($emotional);

        if (isset($emotion)) {
            $query = self::getWhere($query, self::getEmotion($emotion));
        }

        if (isset($expressive)) {
            $query = self::getWhere($query, self::getExpressive($expressive));
        }

        return $query->count();
    }

    /**
     * Gets all emotions for an entity.
     *
     * @param EmotionalContract $emotional Entity to be removed from
     * @return  HasMany|Collection
     */
    public static function getExpressions($emotional)
    {
        return $emotional->relationLoaded('expressions') ? $emotional->expressions : $emotional->expressions();
    }

    /**
     * Gets all emotions for an entity.
     *
     * @param \Illuminate\Database\Eloquent\Collection|Collection $query
     * @param array                                               $conditions
     * @return  HasMany|Collection
     */
    public static function getWhere($query, $conditions)
    {
        if ($query instanceof HasMany) {
            return $query->where($conditions);
        }

        foreach ($conditions as $key => $value) {
            $query = $query->where($key, $value);
        }

        return $query;
    }

    /**
     * Extracts identifiers from emotion.
     *
     * @param EmotionContract|string $emotion Accepts either an entity or an ID.
     * @return  array  In shape ['emotion_id' => int|string]
     */
    public static function getEmotion($emotion): array
    {
        return [
            'emotion_id' => is_object($emotion) ? $emotion->getKey() : $emotion,
        ];
    }

    /**
     * Removes an emotion from an entity.
     *
     * If $expressive is provided, only matching emotions by $expressive are removed. Otherwise, removes all matching
     * emotions.
     *
     * @param EmotionalContract       $emotional  Entity to be removed from
     * @param EmotionContract|string  $emotion    Emotion being removed
     * @param ExpressiveContract|null $expressive Only removes emotions by this entity
     * @return  int  Number of emotions removed
     * @example
     * $service->removeExpression($emotional, 'like');          // all matching emotions are removed
     * $service->removeExpression($emotional, 'like', $expressive); // only $expressive's emotions are removed
     * $service->removeExpression($book, 'read', $user);
     */
    public function removeExpression($emotional, $emotion, $expressive = null): int
    {
        $query = $emotional->expressions()->where(self::getEmotion($emotion));

        if (isset($expressive)) {
            $query->where(self::getExpressive($expressive));
        }

        return $query->delete();
    }

    /**
     * Adds an emotion to an entity.
     *
     * @param EmotionalContract      $emotional  Entity receiving the emotion
     * @param EmotionContract|string $emotion    Emotion being expressed
     * @param ExpressiveContract     $expressive Entity expressing the emotion
     * @return  EmotionalExpressionContract|null   Returns `null` if emotion already exists
     * @example
     * $service->addExpression($emotional, 'like', $expressive);
     */
    public function addExpression($emotional, $emotion, $expressive): ?Model
    {
        if ($this->hasExpression($emotional, $emotion, $expressive)) {
            return null;
        }

        return $emotional->expressions()->create(
            self::getEmotion($emotion) +
            self::getExpressive($expressive) + [
                'emotional_id' => $emotional->getKey(),
                'emotional_type' => get_class($emotional),
            ]);
    }

    /**
     * Gets all emotions for an entity.
     *
     * @param EmotionalContract  $emotional  Entity to be removed from
     * @param ExpressiveContract $expressive Entity to match for
     * @return  Collection  Collection of matching expressive
     * @example
     * $service->addExpression($emotional, 'like', $expressive); // seed $emotional
     * $service->emotionsFor($emotional, $expressive);        // => Collection { $expressive }
     * $service->emotionsFor($book, $user);
     */
    public function emotionsFor($emotional, $expressive): Collection
    {
        $query = self::getExpressions($emotional);
        $query = self::getWhere($query, self::getExpressive($expressive));

        if ($query instanceof HasMany) {
            $query = $query->get();
        }

        return $query
            ->pluck('emotion');
    }

    /**
     * Gets all expressive for an entity.
     *
     * @param EmotionalContract $emotional Entity to be removed from
     * @param EmotionContract   $emotion   Emotion to match for
     * @return  Collection  Collection of matching expressive
     * @example
     * $service->addExpression($emotional, 'like', $expressive); // seed $emotional
     * $emotional->expressivesFor($emotional, 'like');        // => Collection { $expressive }
     * $emotional->expressivesFor($user, 'read');
     */
    public function expressivesFor($emotional, $emotion): Collection
    {
        $query = self::getExpressions($emotional);
        $query = self::getWhere($query, self::getEmotion($emotion));

        if ($query instanceof HasMany) {
            $query = $query->get();
        }

        return $query
            ->pluck('expressive');
    }

    /**
     * Counts matching emotions.
     *
     * @param EmotionalContract           $emotional Entity to count from
     * @param EmotionContract|string|null $emotion   Emotion to match for
     * @return  int  Number of matching emotions
     * @example
     * $service->addExpression($emotional, 'like', $expressive); // seed $emotional
     * $service->countExpressions($emotional, 'like');       // => 1
     * $service->countExpressions($book);
     */
    public function countExpressions($emotional, $emotion = null): int
    {
        $query = self::getExpressions($emotional);

        if ($emotion) {
            $query = self::getWhere($query, self::getEmotion($emotion));
        }

        return $query->count();
    }
}
