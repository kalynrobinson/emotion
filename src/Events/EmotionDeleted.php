<?php

namespace Jackrobin\Emotion\Events;

use Illuminate\Queue\SerializesModels;
use Jackrobin\Contracts\Emotion\Models\Emotion as EmotionContract;

/**
 * Class EmotionSaved.
 *
 * Event fired whenever an Emotion is destroyed.
 */
class EmotionDeleted
{
    use SerializesModels;

    public $emotion;

    /**
     * Create a new event instance.
     *
     * @param EmotionContract $emotion
     */
    public function __construct(EmotionContract $emotion)
    {
        $this->emotion = $emotion;
    }
}
