<?php

namespace Jackrobin\Emotion\Events;

use Illuminate\Queue\SerializesModels;
use Jackrobin\Contracts\Emotion\Models\Pivot\EmotionalExpression as EmotionalExpressionContract;

/**
 * Class ExpressionAdded.
 *
 * Event fired whenever an EmotionalExpression is destroyed.
 */
class ExpressionRemoved
{
    use SerializesModels;

    public $expression;

    /**
     * Create a new event instance.
     *
     * @param EmotionalExpressionContract $expression
     */
    public function __construct(EmotionalExpressionContract $expression)
    {
        $this->expression = $expression;
    }
}
