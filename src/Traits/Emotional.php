<?php

namespace Jackrobin\Emotion\Traits;

use Illuminate\Support\Collection;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Jackrobin\Contracts\Emotion\Models\Emotion as EmotionContract;
use Jackrobin\Contracts\Emotion\Traits\Expressive as ExpressiveContract;
use Jackrobin\Contracts\Emotion\Services\EmotionService as EmotionServiceContract;
use Jackrobin\Contracts\Emotion\Models\Pivot\EmotionalExpression as EmotionalExpressionContract;

/**
 * An entity capable of receiving emotions.
 */
trait Emotional
{
    /**
     * @return  HasMany  Collection of EmotionalExpressions
     */
    public function expressions(): HasMany
    {
        return $this->hasMany(
            app(EmotionalExpressionContract::class),
            'emotional_id'
        );
    }

    /**
     * Adds an expression to the entity.
     *
     * @param EmotionContract|string $emotion    Emotion to add
     * @param ExpressiveContract     $expressive Who is adding the emotion
     * @return  mixed  Success
     * @example
     * $emotional->addExpression('like', $user);
     * $book->addExpression('read', $user);
     */
    public function addExpression($emotion, $expressive)
    {
        return app(EmotionServiceContract::class)->addExpression($this, $emotion, $expressive);
    }

    /**
     * Removes an expression from the entity.
     *
     * If $expressive is provided, only expressions by $expressive are removed. Otherwise, removes all matching
     * expressions.
     *
     * @param EmotionContract|string $emotion Emotion to remove
     * @param   ?ExpressiveContract          $expressive   User to remove by
     * @return  int  Number of removed expressions
     * @example
     *          $emotional->removeExpression('like');        // removes all "like"s
     *          $emotional->removeExpression('like', $user); // removes only "like" by $user
     *          $book->removeExpression('read', $user);
     */
    public function removeExpression($emotion, $expressive = null): int
    {
        return app(EmotionServiceContract::class)->removeExpression($this, $emotion, $expressive);
    }

    /**
     * Adds or removes an expression from the entity.
     *
     * @param EmotionContract|string $emotion
     * @param ExpressiveContract     $expressive
     * @return  mixed  Success
     * @example
     * $emotional->toggleExpression('like', $user); // $emotional is now "liked"
     * $emotional->toggleExpression('like', $user); // $emotional is no longer "liked"
     * $book->toggleExpression('read', $user);
     */
    public function toggleExpression($emotion, $expressive)
    {
        return app(EmotionServiceContract::class)->toggleExpression($this, $emotion, $expressive);
    }

    /**
     * Removes expressions from the entity.
     *
     * If $expressive is provided, only expressions by $expressive are removed. Otherwise, removes all expressions.
     *
     * @param   ?ExpressiveContract  $expressive
     * @return  int  Number of removed expressions
     * @example
     *          $emotional->removeExpressions();      // all expressions are removed
     *          $emotional->removeExpressions($user); // only $user's expressions are removed
     *          $$book->removeExpressions($user);
     */
    public function removeExpressions($expressive = null): int
    {
        return app(EmotionServiceContract::class)->removeExpressions($this, $expressive);
    }

    /**
     * Checks if entity has an expression.
     *
     * If $expressive is provided, looks for $emotion by $expressive.
     *
     * @param EmotionContract|string $emotion
     * @param   ?ExpressiveContract         $expressive
     * @return  int  Number of matching expressions
     * @example
     *          $emotional->addExpression('like', $user);       // seed $emotional
     *
     * $emotional->hasExpression('like');              // => true
     * $emotional->hasExpression('like', $other_user); // => false
     * $book->hasExpression('read', $user);
     */
    public function hasExpression($emotion, $expressive = null): int
    {
        return app(EmotionServiceContract::class)->hasExpression($this, $emotion, $expressive);
    }

    /**
     * Counts matching expressions.
     *
     * @param EmotionContract|string $emotion
     * @return  int  Number of matching expressions
     * @example
     * $emotional->addExpression('like', $user); // seed $emotional
     *
     * $emotional->countExpressions('like');     // => 1
     * $book->countExpressions('read');
     */
    public function countExpressions($emotion): int
    {
        return app(EmotionServiceContract::class)->countExpressions($this, $emotion);
    }

    /**
     * Gets all expressives for an emotion.
     *
     * @param EmotionContract|string $emotion
     * @return  Collection  Collection of matching expressive
     * @example
     * $emotional->addExpression('like', $user); // seed $emotional
     * $emotional->expressivesFor('like');       // => Collection { $user }
     * $book->expressivesFor('read');
     */
    public function expressivesFor($emotion): Collection
    {
        return app(EmotionServiceContract::class)->expressivesFor($this, $emotion);
    }

    /**
     * Gets all emotions for an expressive.
     *
     * @param ExpressiveContract $expressive
     * @return  Collection  Collection of matching emotions
     * @example
     * $emotional->addExpression('like', $user); // seed $emotional
     * $emotional->emotionsFor($user);           // => Collection { 'like' }
     * $book->emotionsFor($user);           // => Collection { 'like' }
     */
    public function emotionsFor($expressive): Collection
    {
        return app(EmotionServiceContract::class)->emotionsFor($this, $expressive);
    }
}
