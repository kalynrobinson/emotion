<?php

namespace Jackrobin\Emotion\Models;

use Spatie\Sluggable\HasSlug;
use Spatie\Sluggable\SlugOptions;
use Illuminate\Database\Eloquent\Model;
use Jackrobin\Emotion\Events\EmotionSaved;
use Jackrobin\Emotion\Events\EmotionDeleted;
use Jackrobin\Contracts\Emotion\Models\Emotion as EmotionContract;

class Emotion extends Model implements EmotionContract
{
    use HasSlug;

    /** {@inheritdoc} */
    public static $rules = [
        'emotion_id' => 'max:255',
        'name' => 'required|max:255',
    ];
    /** {@inheritdoc} */
    public $incrementing = false;
    /** {@inheritdoc} */
    protected $primaryKey = 'emotion_id';
    /** {@inheritdoc} */
    protected $fillable = [
        'emotion_id',
        'name',
    ];
    /** {@inheritdoc} */
    protected $dispatchesEvents = [
        'saved' => EmotionSaved::class,
        'deleted' => EmotionDeleted::class,
    ];

    /** {@inheritdoc} */
    public function getRouteKeyName(): string
    {
        return 'emotion_id';
    }

    /** {@inheritdoc} */
    public function getSlugOptions(): SlugOptions
    {
        return SlugOptions::create()
            ->generateSlugsFrom('name')
            ->saveSlugsTo('emotion_id');
    }
}
