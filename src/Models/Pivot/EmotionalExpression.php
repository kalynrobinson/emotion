<?php

namespace Jackrobin\Emotion\Models\Pivot;

use Illuminate\Database\Eloquent\Model;
use Jackrobin\Emotion\Events\ExpressionAdded;
use Jackrobin\Emotion\Events\ExpressionRemoved;
use Illuminate\Database\Eloquent\Relations\MorphTo;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Jackrobin\Contracts\Emotion\Models\Emotion as EmotionContract;
use Jackrobin\Contracts\Emotion\Models\Pivot\EmotionalExpression as EmotionalExpressionContract;

class EmotionalExpression extends Model implements EmotionalExpressionContract
{
    public $table = 'emotional_expressions';

    public $fillable = [
        'emotion_id',
        'expressive_id',
        'emotional_id',
        'emotional_type',
        'expressive_type',
    ];

    /** {@inheritdoc} */
    protected $dispatchesEvents = ['saved' => ExpressionAdded::class, 'deleted' => ExpressionRemoved::class];

    public function emotion(): BelongsTo
    {
        return $this->belongsTo(app(EmotionContract::class), 'emotion_id', 'emotion_id');
    }

    public function emotional(): MorphTo
    {
        return $this->morphTo('emotional', 'emotional_type', 'emotional_id');
    }

    public function expressive(): MorphTo
    {
        return $this->morphTo('expressive', 'expressive_type', 'expressive_id');
    }
}
