<?php

namespace Jackrobin\Emotion;

use Illuminate\Support\ServiceProvider;

class EmotionServiceProvider extends ServiceProvider
{
    /** {@inheritdoc} */
    public function boot(): void
    {
        $this->registerObservers();
        $this->registerPublishes();
        $this->registerMigrations();
    }

    /** {@inheritdoc} */
    protected function registerObservers(): void
    {
//        $this->app->make(EmotionContract::class)->observe(EmotionObserver::class);
    }

    /** {@inheritdoc} */
    protected function registerPublishes(): void
    {
        $this->publishes([
            __DIR__.'/../database/migrations' => database_path('migrations'),
        ], 'migrations');

        $this->publishes([
            __DIR__.'/../config/emotion.php' => config_path('emotion.php'),
        ], 'config');
    }

    /** {@inheritdoc} */
    protected function registerMigrations(): void
    {
        $this->loadMigrationsFrom(__DIR__.'/../database/migrations');
    }

    /** {@inheritdoc} */
    public function register(): void
    {
        $this->registerContracts();
    }

    /** {@inheritdoc} */
    protected function registerContracts(): void
    {
        $contracts = [
            'Jackrobin\Contracts\Emotion\Models\Pivot\EmotionalExpression' => 'Jackrobin\Emotion\Models\Pivot\EmotionalExpression',
            'Jackrobin\Contracts\Emotion\Models\Emotion' => 'Jackrobin\Emotion\Models\Emotion',
            'Jackrobin\Contracts\Emotion\Services\EmotionService' => 'Jackrobin\Emotion\Services\EmotionService',
            'Jackrobin\Contracts\Emotion\Traits\Emotional' => 'Jackrobin\Emotion\Traits\Emotional',
            'Jackrobin\Contracts\Emotion\Traits\Expressive' => 'Jackrobin\Emotion\Traits\Expressive',
        ];

        foreach ($contracts as $key => $value) {
            $this->app->bind($key, $value);
        }
    }
}
