<?php

namespace Jackrobin\Contracts\Emotion\Traits;

use Illuminate\Database\Eloquent\Relations\HasMany;
use Jackrobin\Contracts\Emotion\Models\Emotion as EmotionContract;
use Jackrobin\Contracts\Emotion\Traits\Emotional as EmotionalContract;

/**
 * Interface Expressive.
 *
 * Entity capable of giving emotions,
 */
interface Expressive
{
    /**
     * @return  HasMany  Collection of EmotionalExpressions
     */
    public function expressed(): HasMany;

    /**
     * Adds an expression to an entity.
     *
     * @param EmotionContract|string $emotion   Emotion to add
     * @param EmotionalContract      $emotional Entity to receive the emotion
     * @return  mixed  Success
     * @example
     * $expressive->addExpressionTo('like', $emotional);
     * $user->addExpression('read', $book);
     */
    public function addExpressionTo($emotion, $emotional);

    /**
     * Removes an expression from an entity.
     *
     * @param EmotionContract|string $emotion   Emotion to remove
     * @param EmotionalContract      $emotional Entity to remove from
     * @return  int  Number of emotions removed
     * @example
     * $expressive->removeExpressionFrom('like', $emotional);
     * $user->removeExpression('read');
     */
    public function removeExpressionFrom($emotion, $emotional): int;

    /**
     * Adds or removes an expression from an entity.
     *
     * @param EmotionContract|string $emotion   Emotion to toggle
     * @param EmotionalContract      $emotional Entity to toggle on
     * @return  mixed  Success
     * @example
     * $expressive->toggleExpression('like', $emotional); // $emotional is now "liked"
     * $expressive->toggleExpression('like', $emotional); // $emotional is no longer "liked"
     * $user->toggleExpression('read', $book);
     */
    public function toggleExpressionFor($emotion, $emotional);

    /**
     * Checks if the entity has expressed an emotion to an emotional.
     *
     * @param EmotionContract|string $emotion   Emotion to remove
     * @param EmotionalContract      $emotional Entity to remove from
     * @return  mixed  Success
     * @example
     * $expressive->hasExpressed('like', $emotional);     // false
     * $expressive->addExpressionTo('like', $emotional);  // seed $emotional
     * $expressive->hasExpressed('like', $emotional);     // true
     * $user->hasExpressed('read', $book);
     */
    public function hasExpressed($emotion, $emotional);
}
