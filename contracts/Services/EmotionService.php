<?php

namespace Jackrobin\Contracts\Emotion\Services;

use Illuminate\Support\Collection;
use Illuminate\Database\Eloquent\Model;
use Jackrobin\Contracts\Emotion\Models\Emotion as EmotionContract;
use Jackrobin\Contracts\Emotion\Traits\Emotional as EmotionalContract;
use Jackrobin\Contracts\Emotion\Traits\Expressive as ExpressiveContract;
use Jackrobin\Contracts\Emotion\Models\Pivot\EmotionalExpression as EmotionalExpressionContract;

/**
 * Interface EmotionService.
 *
 * Handles all emotion-related processes, e.g. adding and removing emotions.
 *
 * EmotionService can be invoked directly.
 *
 * @example
 * $service = app(EmotionServiceContract::class); // or new EmotionService()
 * $service->hasExpression($emotion, $emotion, $expressive);
 * $service->addExpression($emotional, 'like', $expressive);
 * $service->addExpression($book, 'read', $user);
 */
interface EmotionService
{
    /**
     * Extracts identifiers from emotion.
     *
     * @param EmotionContract|string $emotion Accepts either an entity or an ID.
     * @return  array  In shape ['emotion_id' => int|string]
     */
    public static function getEmotion($emotion): array;

    /**
     * Extracts identifiers from expressive.
     *
     * @param ExpressiveContract $expressive
     * @return  array  In shape ['expressive_id' => int|string, 'expressive_type' => string]
     */
    public static function getExpressive($expressive): array;

    /**
     * Adds an emotion to an entity.
     *
     * @param EmotionalContract      $emotional  Entity receiving the emotion
     * @param EmotionContract|string $emotion    Emotion being expressed
     * @param ExpressiveContract     $expressive Entity expressing the emotion
     * @return  EmotionalExpressionContract|null   Returns `null` if emotion already exists
     * @example
     * $service->addExpression($emotional, 'like', $expressive);
     */
    public function addExpression($emotional, $emotion, $expressive): ?Model;

    /**
     * Removes an emotion from an entity.
     *
     * If $expressive is provided, only matching emotions by $expressive are removed. Otherwise, removes all matching
     * emotions.
     *
     * @param EmotionalContract       $emotional  Entity to be removed from
     * @param EmotionContract|string  $emotion    Emotion being removed
     * @param ExpressiveContract|null $expressive Only removes emotions by this entity
     * @return  int  Number of emotions removed
     * @example
     * $service->removeExpression($emotional, 'like');          // all matching emotions are removed
     * $service->removeExpression($emotional, 'like', $expressive); // only $expressive's emotions are removed
     * $service->removeExpression($book, 'read', $user);
     */
    public function removeExpression($emotional, $emotion, $expressive = null): int;

    /**
     * Removes all emotions from an entity.
     *
     * If $expressive is provided, only emotions by $expressive are removed. Otherwise, removes all emotions.
     *
     * @param EmotionalContract       $emotional  Entity to be removed from
     * @param ExpressiveContract|null $expressive Only removes emotions by this entity
     * @return int  Number of emotions removed
     * @example
     * $service->removeExpressions($emotional);              // all emotions are removed
     * $service->removeExpressions($emotional, $expressive); // only $user's emotions are removed
     * $service->removeExpressions($book, $user);
     */
    public function removeExpressions($emotional, $expressive = null): int;

    /**
     * Checks if entity has an emotion.
     *
     * If $expressive is provided, looks for $emotion by $expressive.
     *
     * @param EmotionalContract       $emotional  Entity to be removed from
     * @param EmotionContract         $emotion
     * @param ExpressiveContract|null $expressive Only removes emotions by this entity
     * @return  int  Number of matching emotions
     * @example
     * $service->addExpression($emotional, 'like', $user);       // seed $emotional
     *
     * $service->hasExpression($emotional, 'like');              // => 1
     * $service->hasExpression($emotional, 'like', $user);       // => 1
     * $service->hasExpression($emotional, 'like', $other_user); // => 0
     * $service->hasExpression($book, 'read', $user);
     */
    public function hasExpression($emotional, $emotion, $expressive = null): int;

    /**
     * Adds or removes an emotion from an entity.
     *
     * @param EmotionalContract       $emotional  Entity to be removed from
     * @param EmotionContract         $emotion
     * @param ExpressiveContract|null $expressive Only removes emotions by this entity
     * @return  mixed  Success
     * @example
     * $service->toggleExpression($emotional, 'like', $user); // $emotional is now "liked"
     * $service->toggleExpression($emotional, 'like', $user); // $emotional is no longer "liked"
     * $service->toggleExpression($book, 'read', $user);
     */
    public function toggleExpression($emotional, $emotion, $expressive);

    /**
     * Gets all emotions for an entity.
     *
     * @param EmotionalContract  $emotional  Entity to be removed from
     * @param ExpressiveContract $expressive Entity to match for
     * @return  Collection  Collection of matching expressive
     * @example
     * $service->addExpression($emotional, 'like', $expressive); // seed $emotional
     * $service->emotionsFor($emotional, $expressive);        // => Collection { $expressive }
     * $service->emotionsFor($book, $user);
     */
    public function emotionsFor($emotional, $expressive): Collection;

    /**
     * Gets all expressive for an entity.
     *
     * @param EmotionalContract $emotional Entity to be removed from
     * @param EmotionContract   $emotion   Emotion to match for
     * @return  Collection  Collection of matching expressive
     * @example
     * $service->addExpression($emotional, 'like', $expressive); // seed $emotional
     * $emotional->expressivesFor($emotional, 'like');        // => Collection { $expressive }
     * $emotional->expressivesFor($user, 'read');
     */
    public function expressivesFor($emotional, $emotion): Collection;

    /**
     * Counts matching emotions.
     *
     * @param EmotionalContract           $emotional Entity to count from
     * @param EmotionContract|string|null $emotion   Emotion to match for
     * @return  int  Number of matching emotions
     * @example
     * $service->addExpression($emotional, 'like', $expressive); // seed $emotional
     * $service->countExpressions($emotional, 'like');       // => 1
     * $service->countExpressions($book);
     */
    public function countExpressions($emotional, $emotion = null): int;
}
