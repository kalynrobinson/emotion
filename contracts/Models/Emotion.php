<?php

namespace Jackrobin\Contracts\Emotion\Models;

/**
 * Interface Emotion.
 *
 * Model for types of expressions.
 */
interface Emotion
{
    public function getRouteKeyName(): string;
}
