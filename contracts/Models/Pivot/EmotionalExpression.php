<?php

namespace Jackrobin\Contracts\Emotion\Models\Pivot;

use Illuminate\Database\Eloquent\Relations\MorphTo;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

/**
 * Interface EmotionalExpression.
 *
 * Join model for Emotion, Emotional, and Expressive.
 */
interface EmotionalExpression
{
    /**
     * Fetches the associated Emotion.
     *
     * @return BelongsTo
     */
    public function emotion(): BelongsTo;

    /**
     * Fetches the associated Emotional.
     *
     * @return MorphTo
     */
    public function emotional(): MorphTo;

    /**
     * Fetches the associated Expressive.
     *
     * @return MorphTo
     */
    public function expressive(): MorphTo;
}
