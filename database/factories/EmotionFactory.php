<?php

use Faker\Generator as Faker;
use Jackrobin\Emotion\Models\Emotion;

$factory->define(Emotion::class, function (Faker $faker) {
    return [
        'name' => $faker->word,
    ];
});
