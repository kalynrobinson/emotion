<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEmotionalExpressionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('emotional_expressions', function (Blueprint $table) {
            $table->increments('id');

            $table->morphs('emotional');
            $table->morphs('expressive');
            $table->string('emotion_id');

            $table->foreign('emotion_id')->references('emotion_id')->on('emotions');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('emotional_expressions');
    }
}
