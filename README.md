# 💖 Emotion

[![Latest Version on Packagist][ico-version]][link-packagist]
[![Total Downloads][ico-downloads]][link-downloads]
[![Pipeline][ico-gitlab]][link-gitlab]
[![Coverage][ico-coverage]][link-coverage]
[![StyleCI][ico-styleci]][link-styleci]

Facebook-like reactions for Eloquent models. Let your users express themselves!

## Installation

Via Composer

``` bash
composer require jackrobin/emotion
```

Then publish and run the migrations

``` bash
php artisan vendor:publish --tag=migrations --provider=
php artisan migrate
```

## Usage

``` php
$discussion->receive('love', $user);
$user->express('love', $discussion);
```

## Change log

Please see the [changelog](changelog.md) for more information on what has changed recently.

## Testing

``` bash
$ composer test
```

## Contributing

Please see [contributing.md](contributing.md) for details and a todolist.

## Security

If you discover any security related issues, please email kalynrobinson@gmail.com instead of using the issue tracker.

## Credits

- [Kalyn Robinson][link-author]
- [All Contributors][link-contributors]

## License

MIT. Please see the [license file](license.md) for more information.

[ico-version]: https://img.shields.io/packagist/v/jackrobin/emotion.svg?style=flat-square
[ico-downloads]: https://img.shields.io/packagist/dt/jackrobin/emotion.svg?style=flat-square
[ico-styleci]: https://gitlab.styleci.io/repos/11140273/shield
[ico-gitlab]: https://gitlab.com/kalynrobinson/emotion/badges/develop/pipeline.svg
[ico-coverage]: https://gitlab.com/kalynrobinson/emotion/badges/develop/coverage.svg

[link-packagist]: https://packagist.org/packages/jackrobin/emotion
[link-downloads]: https://packagist.org/packages/jackrobin/emotion
[link-styleci]: https://gitlab.styleci.io/repos/11140273
[link-gitlab]: https://gitlab.com/kalynrobinson/emotion/commits/master
[link-coverage]: https://gitlab.com/kalynrobinson/emotion/commits/master
[link-author]: https://github.com/kalynrobinson
[link-contributors]: ../../contributors
