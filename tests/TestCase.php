<?php

namespace Jackrobin\Emotion\Tests;

use Dotenv\Dotenv;
use Illuminate\Foundation\Application;
use Jackrobin\Emotion\EmotionServiceProvider;
use Orchestra\Testbench\TestCase as BaseTestCase;
use Jackrobin\Emotion\Tests\Database\Seeds\TestSeeder;

abstract class TestCase extends BaseTestCase
{
    protected function setUp(): void
    {
        parent::setUp();

        // migrate
        $this->loadLaravelMigrations();
        $this->artisan('migrate');

        // seed
        $this->withFactories(__DIR__.'/database/factories');
        $this->withFactories(__DIR__.'/../database/factories');
        $this->seed(TestSeeder::class);

        // rollback
        $this->beforeApplicationDestroyed(function () {
            $this->artisan('migrate:rollback');
        });
    }

    protected function getPackageProviders($app): array
    {
        return [
            EmotionServiceProvider::class,
            TestServiceProvider::class,
        ];
    }

    /**
     * Define environment setup.
     *
     * @param Application $app
     *
     * @return void
     */
    protected function getEnvironmentSetUp($app): void
    {
        // If we're not in Travis, load our local .env file
        if (empty(getenv('CI'))) {
            $dotenv = Dotenv::create(__DIR__.'/..');
            $dotenv->load();
        }

        $app['config']->set('emotion.default_models.expressive', Stubs\User::class);
    }
}
