<?php

namespace Jackrobin\Emotion\Tests\Database\Factories;

use Faker\Generator as Faker;
use Jackrobin\Emotion\Tests\Stubs\Discussion;

$factory->define(Discussion::class, function (Faker $faker) {
    return [
        'name' => $faker->word,
    ];
});
