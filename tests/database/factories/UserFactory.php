<?php

use Faker\Generator as Faker;
use Jackrobin\Emotion\Tests\Stubs\User;

$factory->define(User::class, function (Faker $faker) {
    return [
        'name' => $faker->unique()->name,
        'email' => $faker->unique()->email,
        'password' => $faker->password,
    ];
});
