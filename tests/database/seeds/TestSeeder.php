<?php

namespace Jackrobin\Emotion\Tests\Database\Seeds;

use Illuminate\Database\Seeder;

class TestSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call([
            UsersTableSeeder::class,
            EmotionsTableSeeder::class,
        ]);
    }
}
