<?php

namespace Jackrobin\Emotion\Tests\Database\Seeds;

use Illuminate\Database\Seeder;
use Jackrobin\Emotion\Models\Emotion;

class EmotionsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // create categories
        factory(Emotion::class)->create(['name' => 'like']);
        factory(Emotion::class)->create(['name' => 'love']);
    }
}
