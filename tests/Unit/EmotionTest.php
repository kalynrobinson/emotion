<?php

namespace Jackrobin\Emotion\Tests\Unit;

use Illuminate\Support\Facades\Event;
use Jackrobin\Emotion\Models\Emotion;
use Jackrobin\Emotion\Tests\TestCase;
use Jackrobin\Emotion\Events\EmotionSaved;
use Jackrobin\Emotion\Events\EmotionDeleted;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class EmotionTest extends TestCase
{
    use DatabaseTransactions;

    /**
     * @test
     */
    public function it_creates_emotion(): void
    {
        Event::fake([
            EmotionSaved::class,
        ]);

        $name = 'Like';
        $count = Emotion::count();
        $emotion = Emotion::create(['name' => $name]);

        // has inserted new record
        $this->assertEquals(
            $count + 1,
            Emotion::count()
        );

        $emotion = Emotion::find($emotion->emotion_id);

        // with correct data
        $this->assertEquals(
            $name,
            $emotion->name
        );

        Event::assertDispatched(EmotionSaved::class);
    }

    /**
     * @test
     * @throws \Illuminate\Database\QueryException
     */
    public function it_does_not_create_emotion(): void
    {
        Event::fake([
            EmotionSaved::class,
        ]);

        $this->expectException(\Illuminate\Database\QueryException::class);
        Emotion::create();

        Event::assertNptDispatched(EmotionSaved::class);
    }

    /**
     * @test
     */
    public function it_generates_slug(): void
    {
        $slug = 'alongslugname';
        $emotion = new Emotion(['name' => $slug]);
        $emotion->save();

        $this->assertEquals(
            $slug,
            $emotion->emotion_id
        );
    }

    /**
     * @test
     */
    public function it_has_route_key(): void
    {
        $slug = 'alongslugname';
        $emotion = new Emotion(['name' => $slug]);
        $key = $emotion->getRouteKeyName();

        $this->assertEquals(
            'emotion_id',
            $key
        );
    }

    /**
     * @test
     */
    public function it_destroys_emotion(): void
    {
        Event::fake([
            EmotionDeleted::class,
        ]);

        $name = 'Like';
        $emotion = Emotion::create(['name' => $name]);
        $count = Emotion::count();

        $emotion->delete();

        // has inserted new record
        $this->assertEquals(
            $count - 1,
            Emotion::count()
        );

        Event::assertDispatched(EmotionDeleted::class);
    }
}
