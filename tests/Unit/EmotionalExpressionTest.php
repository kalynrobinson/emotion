<?php

namespace Jackrobin\Emotion\Tests\Unit;

use Illuminate\Support\Facades\Event;
use Jackrobin\Emotion\Models\Emotion;
use Jackrobin\Emotion\Tests\TestCase;
use Jackrobin\Emotion\Tests\Stubs\User;
use Jackrobin\Emotion\Events\ExpressionAdded;
use Jackrobin\Emotion\Tests\Stubs\Discussion;
use Jackrobin\Emotion\Events\ExpressionRemoved;
use Jackrobin\Emotion\Models\Pivot\EmotionalExpression;
use Jackrobin\Contracts\Emotion\Models\Emotion as EmotionContract;
use Jackrobin\Contracts\Emotion\Traits\Emotional as EmotionalContract;
use Jackrobin\Contracts\Emotion\Traits\Expressive as ExpressiveContract;

class EmotionalExpressionTest extends TestCase
{
    protected $user;
    protected $other_user;
    protected $emotion;
    protected $other_emotion;
    protected $discussion;
    protected $join;
    protected $count;

    /**
     * Sanity check.
     *
     * @test
     */
    public function it_creates_emotional_emotion(): void
    {
        Event::fake();

        $count = EmotionalExpression::count();
        EmotionalExpression::create([
            'emotion_id' => $this->emotion->getKey(),
            'emotional_id' => $this->discussion->getKey(),
            'expressive_id' => $this->user->getKey(),
            'emotional_type' => get_class($this->discussion),
            'expressive_type' => get_class($this->user),
        ]);

        $this->assertEquals(
            $count + 1,
            EmotionalExpression::count()
        );

        Event::assertDispatched(ExpressionAdded::class);
    }

    /**
     * @test
     */
    public function it_belongs_to_emotion(): void
    {
        $this->join->load('emotion');

        // it belongs to an emotional
        $this->assertInstanceOf(
            EmotionContract::class,
            $this->join->emotion
        );

        // it belongs to the correct emotional
        $this->assertEquals($this->emotion->getKey(), $this->join->emotion->getKey());
    }

    /**
     * @test
     */
    public function it_belongs_to_emotional(): void
    {
        $this->join->load('emotional');

        // it belongs to an emotional
        $this->assertInstanceOf(
            EmotionalContract::class,
            $this->join->emotional
        );

        // it belongs to the correct emotional
        $this->assertEquals($this->emotion->getKey(), $this->join->emotion->getKey());
    }

    /**
     * @test
     */
    public function it_belongs_to_expressive(): void
    {
        $this->join->load('expressive');

        // it belongs to an emotional
        $this->assertInstanceOf(
            ExpressiveContract::class,
            $this->join->expressive
        );

        // it belongs to the correct emotional
        $this->assertEquals($this->user->getKey(), $this->join->expressive->getKey());
    }

    /**
     * Sanity check.
     *
     * @test
     */
    public function it_deletes_emotional_expression(): void
    {
        Event::fake();

        $count = EmotionalExpression::count();

        $this->join->delete();

        $this->assertEquals(
            $count - 1,
            EmotionalExpression::count()
        );

        Event::assertDispatched(ExpressionRemoved::class);
    }

    protected function setUp(): void
    {
        parent::setUp();

        $this->user = User::first();
        $this->other_user = User::find(2);
        $this->emotion = Emotion::first();
        $this->other_emotion = Emotion::find(2);

        $this->discussion = Discussion::create(['name' => 'Discussion']);
        $this->join = EmotionalExpression::create([
            'emotion_id' => $this->emotion->getKey(),
            'emotional_id' => $this->discussion->getKey(),
            'expressive_id' => $this->user->getKey(),
            'emotional_type' => get_class($this->discussion),
            'expressive_type' => get_class($this->user),
        ]);
    }
}
