<?php

namespace Jackrobin\Emotion\Tests\Unit;

use Illuminate\Support\Collection;
use Jackrobin\Emotion\Models\Emotion;
use Jackrobin\Emotion\Tests\TestCase;
use Jackrobin\Emotion\Tests\Stubs\User;
use Jackrobin\Emotion\Tests\Stubs\Discussion;
use Jackrobin\Emotion\Models\Pivot\EmotionalExpression;

class EmotionalTest extends TestCase
{
    protected $user;
    protected $other_user;
    protected $emotion;
    protected $other_emotion;
    protected $discussion;
    protected $count;

    /**
     * Sanity check.
     *
     * @test
     */
    public function it_creates_emotional(): void
    {
        $count = Discussion::count();
        Discussion::create(['name' => 'Discussion']);

        $this->assertEquals(
            $count + 1,
            Discussion::count()
        );
    }

    /**
     * @test
     */
    public function it_has_expressions(): void
    {
        $count = $this->discussion->expressions()->count();
        $this->seedExpressions();
        $discussion = Discussion::find($this->discussion->discussion_id);

        // it has emotions
        $this->assertInstanceOf(
            Collection::class,
            $discussion->expressions
        );

        // it has enough emotions
        $this->assertEquals(
            $count + 1,
            $discussion->expressions()->count()
        );

        // it has the correct emotions
        $emotion = $discussion->expressions->first();
        $this->assertEquals([
            'emotion_id' => $this->emotion->getKey(),
            'emotional_id' => $this->discussion->getKey(),
            'expressive_id' => $this->user->getKey(),
            'emotional_type' => get_class($this->discussion),
            'expressive_type' => get_class($this->user),
        ], [
            'emotion_id' => $emotion->emotion_id,
            'emotional_id' => $emotion->emotional_id,
            'expressive_id' => $emotion->expressive_id,
            'emotional_type' => $emotion->emotional_type,
            'expressive_type' => $emotion->expressive_type,
        ]);
    }

    protected function seedExpressions($emotional = null, $emotion = null, $expressive = null)
    {
        $emotional = $emotional ?: $this->discussion;
        $emotion = $emotion ?: $this->emotion;
        $expressive = $expressive ?: $this->user;
        EmotionalExpression::create([
            'emotion_id' => $emotion->getKey(),
            'emotional_id' => $emotional->getKey(),
            'expressive_id' => $expressive->getKey(),
            'emotional_type' => get_class($emotional),
            'expressive_type' => get_class($expressive),
        ]);
    }

    /**
     * @test
     */
    public function it_receives_emotion(): void
    {
        $this->seedExpressions($this->discussion, $this->emotion, $this->other_user);
        $count = $this->discussion->expressions()->count();

        $this->discussion->addExpression($this->emotion, $this->user);
        $discussion = Discussion::find($this->discussion->discussion_id);

        $this->assertEquals(
            $count + 1,
            $discussion->expressions()->count()
        );
    }

    /**
     * @test
     */
    public function it_does_not_receive_duplicate_emotion(): void
    {
        $this->seedExpressions($this->discussion, $this->emotion, $this->user);
        $discussion = Discussion::find($this->discussion->discussion_id);
        $count = $discussion->expressions()->count();

        $this->assertEquals(
            1,
            $discussion->expressions()->count()
        );

        $discussion->addExpression($this->emotion, $this->user);

        $this->assertEquals(
            $count,
            $discussion->expressions()->count()
        );
    }

    /**
     * @test
     */
    public function it_removes_emotion(): void
    {
        $this->seedExpressions($this->discussion, $this->emotion, $this->user);
        $discussion = Discussion::find($this->discussion->discussion_id);
        $count = $discussion->expressions()->count();

        $this->assertEquals(
            1,
            $discussion->expressions()->count()
        );

        $discussion->removeExpression($this->emotion, $this->user);

        $this->assertEquals(
            $count - 1,
            $discussion->expressions()->count()
        );
    }

    /**
     * @test
     */
    public function it_removes_expressions(): void
    {
        $this->seedExpressions($this->discussion, $this->emotion, $this->user);
        $this->seedExpressions($this->discussion, $this->emotion, $this->other_user);
        $this->seedExpressions($this->discussion, $this->other_emotion, $this->user);

        // react to and re-fetch
        $this->discussion->removeExpressions();

        $this->assertEquals(
            0,
            $this->discussion->expressions()->count()
        );
    }

    /**
     * @test
     */
    public function it_removes_emotions_by_user(): void
    {
        $this->seedExpressions($this->discussion, $this->emotion, $this->user);
        $this->seedExpressions($this->discussion, $this->emotion, $this->other_user);
        $this->seedExpressions($this->discussion, $this->other_emotion, $this->user);

        // react to and re-fetch
        $this->discussion->removeExpressions($this->user);

        $this->assertEquals(
            1,
            $this->discussion->expressions()->count()
        );
    }

    /**
     * @test
     */
    public function it_toggles_emotion_on(): void
    {
        $this->seedExpressions($this->discussion, $this->emotion, $this->other_user);
        $count = $this->discussion->expressions()->count();

        $this->discussion->toggleExpression($this->emotion, $this->user);
        $discussion = Discussion::find($this->discussion->discussion_id);

        $this->assertEquals(
            $count + 1,
            $discussion->expressions()->count()
        );
    }

    /**
     * @test
     */
    public function it_toggles_emotion_off(): void
    {
        $this->seedExpressions($this->discussion, $this->emotion, $this->user);
        $count = $this->discussion->expressions()->count();

        $this->discussion->toggleExpression($this->emotion, $this->user);
        $discussion = Discussion::find($this->discussion->discussion_id);

        $this->assertEquals(
            $count - 1,
            $discussion->expressions()->count()
        );
    }

    /**
     * @test
     */
    public function it_has_emotion(): void
    {
        $this->seedExpressions($this->discussion, $this->emotion, $this->user);
        $this->seedExpressions($this->discussion, $this->emotion, $this->other_user);
        $this->seedExpressions($this->discussion, $this->other_emotion, $this->user);
        $discussion = Discussion::find($this->discussion->discussion_id);
        $count = 1;

        $received = $discussion->hasExpression($this->emotion, $this->user);

        $this->assertEquals(
            $count,
            $received
        );
    }

    /**
     * @test
     */
    public function it_does_not_have_emotion(): void
    {
        $this->seedExpressions($this->discussion, $this->other_emotion, $this->user);
        $this->seedExpressions($this->discussion, $this->emotion, $this->other_user);
        $received = $this->discussion->hasExpression($this->emotion, $this->user);

        $this->assertEquals(
            0,
            $received
        );
    }

    /**
     * @test
     */
    public function it_counts_expressions(): void
    {
        $this->seedExpressions($this->discussion, $this->other_emotion, $this->user);
        $this->seedExpressions($this->discussion, $this->emotion, $this->other_user);
        $received = $this->discussion->countExpressions($this->emotion);

        $this->assertEquals(
            1,
            $received
        );
    }

    /**
     * @test
     */
    public function it_gets_expressive_for(): void
    {
        $this->seedExpressions($this->discussion, $this->emotion, $this->user);
        $this->seedExpressions($this->discussion, $this->emotion, $this->other_user);
        $received = $this->discussion->expressivesFor($this->emotion);

        $this->assertEquals(
            2,
            $received->count()
        );

        $this->assertEquals(
            [$this->user->getKey(), $this->other_user->getKey()],
            $received->pluck('id')->toArray()
        );
    }

    /**
     * @test
     */
    public function it_gets_emotions_for(): void
    {
        $this->seedExpressions($this->discussion, $this->emotion, $this->user);
        $this->seedExpressions($this->discussion, $this->other_emotion, $this->user);
        $this->seedExpressions($this->discussion, $this->emotion, $this->other_user);
        $received = $this->discussion->emotionsFor($this->user);

        $this->assertEquals(
            2,
            $received->count()
        );

        $this->assertEquals(
            [$this->emotion->getKey(), $this->other_emotion->getKey()],
            $received->pluck('emotion_id')->toArray()
        );
    }

    protected function setUp(): void
    {
        parent::setUp();

        $this->user = User::first();
        $this->other_user = User::all()->get(1);
        $this->emotion = Emotion::first();
        $this->other_emotion = Emotion::all()->get(1);

        $this->discussion = Discussion::create(['name' => 'Discussion']);
    }
}
