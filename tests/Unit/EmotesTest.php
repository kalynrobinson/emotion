<?php

namespace Jackrobin\Emotion\Tests\Unit;

use Illuminate\Support\Collection;
use Jackrobin\Emotion\Models\Emotion;
use Jackrobin\Emotion\Tests\TestCase;
use Jackrobin\Emotion\Tests\Stubs\User;
use Jackrobin\Emotion\Tests\Stubs\Discussion;
use Jackrobin\Emotion\Models\Pivot\EmotionalExpression;

class EmotesTest extends TestCase
{
    protected $user;
    protected $other_user;
    protected $emotion;
    protected $other_emotion;
    protected $discussion;
    protected $count;

    /**
     * @test
     */
    public function it_has_expressed_relationship(): void
    {
        $count = $this->user->expressed()->count();
        $this->seedExpressions();
        $user = User::find($this->user->getKey());

        // it has emotions
        $this->assertInstanceOf(
            Collection::class,
            $user->expressed
        );

        // it has enough emotions
        $this->assertEquals(
            $count + 1,
            $user->expressed()->count()
        );

        // it has the correct emotions
        $emotion = $user->expressed->first();
        $this->assertEquals([
            'emotion_id' => $this->emotion->getKey(),
            'emotional_id' => $this->discussion->getKey(),
            'expressive_id' => $this->user->getKey(),
            'emotional_type' => get_class($this->discussion),
            'expressive_type' => get_class($this->user),
        ], [
            'emotion_id' => $emotion->emotion_id,
            'emotional_id' => $emotion->emotional_id,
            'expressive_id' => $emotion->expressive_id,
            'emotional_type' => $emotion->emotional_type,
            'expressive_type' => $emotion->expressive_type,
        ]);
    }

    protected function seedExpressions($emotional = null, $emotion = null, $expressive = null)
    {
        $emotional = $emotional ?: $this->discussion;
        $emotion = $emotion ?: $this->emotion;
        $expressive = $expressive ?: $this->user;
        EmotionalExpression::create([
            'emotion_id' => $emotion->getKey(),
            'emotional_id' => $emotional->getKey(),
            'expressive_id' => $expressive->getKey(),
            'emotional_type' => get_class($emotional),
            'expressive_type' => get_class($expressive),
        ]);
    }

    /**
     * @test
     */
    public function it_expresses_emotion(): void
    {
        $this->seedExpressions($this->discussion, $this->emotion, $this->other_user);
        $count = $this->user->expressed()->count();

        $this->user->addExpressionTo($this->emotion, $this->discussion);
        $user = User::find($this->user->getKey());

        $this->assertEquals(
            $count + 1,
            $user->expressed()->count()
        );
    }

    /**
     * @test
     */
    public function it_does_not_express_duplicate_emotion(): void
    {
        $this->seedExpressions($this->discussion, $this->emotion, $this->user);
        $count = $this->user->expressed()->count();

        $this->user->addExpressionTo($this->emotion, $this->discussion);
        $user = User::find($this->user->getKey());

        $this->assertEquals(
            $count,
            $user->expressed()->count()
        );
    }

    /**
     * @test
     */
    public function it_removes_emotion(): void
    {
        $this->seedExpressions($this->discussion, $this->emotion, $this->user);
        $count = $this->user->expressed()->count();

        $this->user->removeExpressionFrom($this->emotion, $this->discussion);
        $user = User::find($this->user->getKey());

        $this->assertEquals(
            $count - 1,
            $user->expressed()->count()
        );
    }

    /**
     * @test
     */
    public function it_toggles_emotion_on(): void
    {
        $this->seedExpressions($this->discussion, $this->emotion, $this->other_user);
        $count = $this->user->expressed()->count();

        $this->user->toggleExpressionFor($this->emotion, $this->discussion);
        $user = User::find($this->user->getKey());

        $this->assertEquals(
            $count + 1,
            $user->expressed()->count()
        );
    }

    /**
     * @test
     */
    public function it_toggles_emotion_off(): void
    {
        $this->seedExpressions($this->discussion, $this->emotion, $this->user);
        $count = $this->user->expressed()->count();

        $this->user->toggleExpressionFor($this->emotion, $this->discussion);
        $user = User::find($this->user->getKey());

        $this->assertEquals(
            $count - 1,
            $user->expressed()->count()
        );
    }

    /**
     * @test
     */
    public function it_has_expressed(): void
    {
        $this->seedExpressions($this->discussion, $this->emotion, $this->user);
        $received = $this->user->hasExpressed($this->emotion, $this->discussion);

        $this->assertEquals(
            1,
            $received
        );
    }

    protected function setUp(): void
    {
        parent::setUp();

        $this->user = User::first();
        $this->other_user = User::all()->get(1);
        $this->emotion = Emotion::first();
        $this->other_emotion = Emotion::all()->get(1);

        $this->discussion = Discussion::create(['name' => 'Discussion']);
    }
}
