<?php

namespace Jackrobin\Emotion\Tests\Unit;

use Jackrobin\Emotion\Models\Emotion;
use Jackrobin\Emotion\Tests\TestCase;
use Jackrobin\Emotion\Tests\Stubs\User;
use Jackrobin\Emotion\Tests\Stubs\Discussion;
use Jackrobin\Emotion\Services\EmotionService;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Jackrobin\Emotion\Tests\DataProviders\EmotionServiceDataProvider;

class EmotionServiceTest extends TestCase
{
    use DatabaseTransactions, EmotionServiceDataProvider;

    protected $emotionService;
    protected $user;
    protected $other_user;
    protected $emotion;
    protected $other_emotion;
    protected $discussion;

    /**
     * @test
     * @dataProvider emotionProvider
     * @param $emotion
     * @param $expressive
     * @param $expected
     */
    public function it_adds_emotion($emotion, $expressive, $expected): void
    {
        $count = $this->discussion->expressions()->count();
        $this->emotionService->addExpression($this->discussion, $emotion, $expressive);
        $discussion = Discussion::find($this->discussion->getKey());

        $this->assertEquals(
            $count + 1,
            $discussion->expressions()->count()
        );

        $join = $discussion->expressions->first();
        $this->assertEquals($expected + [
                'emotional_id' => (string) $this->discussion->getKey(),
                'emotional_type' => get_class($this->discussion),
            ], [
            'emotional_id' => $join->emotional_id,
            'emotional_type' => $join->emotional_type,
            'expressive_id' => $join->expressive_id,
            'expressive_type' => $join->expressive_type,
            'emotion_id' => $join->emotion_id,
        ]);
    }

    /**
     * @test
     * @depends      it_adds_emotion
     * @dataProvider removeExpressionProvider
     * @param $emotion
     * @param $expressive
     * @param $expected
     */
    public function it_removes_emotion($emotion, $expressive, $expected): void
    {
        $this->seedExpressions($this->discussion, $this->emotion, $expressive ?: $this->user);

        $discussion = Discussion::find($this->discussion->getKey());
        $count = $discussion->expressions->count();

        $removed = $this->emotionService->removeExpression($discussion, $emotion, $expressive);
        $discussion = Discussion::find($discussion->getKey());

        $this->assertEquals($expected * -1, $removed);
        $this->assertEquals(
            $count + $expected,
            $discussion->expressions()->count()
        );
    }

    protected function seedExpressions($emotional = null, $emotion = null, $expressive = null, $other_emotion = null, $other_expressive = null)
    {
        $emotional = $emotional ?: $this->discussion;
        $emotion = $emotion ?: $this->emotion;
        $expressive = $expressive ?: $this->user;
        $other_emotion = $other_emotion ?: $this->other_emotion;
        $other_expressive = $other_expressive ?: $this->other_user;
        $this->emotionService->addExpression($emotional, $emotion, $expressive, $other_emotion, $other_expressive);
        $this->emotionService->addExpression($emotional, $other_emotion, $expressive, $other_emotion, $other_expressive);
        $this->emotionService->addExpression($emotional, $emotion, $other_expressive, $other_emotion, $other_expressive);
    }

    /**
     * @test
     * @depends      it_adds_emotion
     * @dataProvider removeExpressionsProvider
     * @param $expressive
     * @param $expected
     */
    public function it_removes_emotions($expressive, $expected): void
    {
        $this->seedExpressions($this->discussion, $this->emotion, $expressive ?? $this->user);

        $discussion = Discussion::find($this->discussion->getKey());
        $count = $discussion->expressions->count();

        $discussion->unsetRelation('expressions');
        $removed = $this->emotionService->removeExpressions($discussion, $expressive);
        $discussion = Discussion::find($discussion->getKey());

        $this->assertEquals($expected * -1, $removed);
        $this->assertEquals(
            $count + $expected,
            $discussion->expressions()->count()
        );
    }

    /**
     * @test
     * @depends      it_adds_emotion
     * @dataProvider hasExpressionProvider
     * @param $emotion
     * @param $expressive
     * @param $expected
     */
    public function it_has_emotion($emotion, $expressive, $expected): void
    {
        $this->seedExpressions($this->discussion, $this->emotion, $expressive);
        $discussion = Discussion::find($this->discussion->getKey());
        $count = $this->emotionService->hasExpression($discussion, $emotion, $expressive);

        $this->assertEquals($expected, $count);
    }

    /**
     * @test
     * @depends      it_adds_emotion
     * @dataProvider emotionsForProvider
     * @param $expressive
     * @param $expected
     */
    public function it_gets_emotions_for($expressive, $expected): void
    {
        $this->seedExpressions($this->discussion, $this->emotion, $this->user);
        $discussion = Discussion::find($this->discussion->getKey());
        $received = $this->emotionService->emotionsFor($discussion, $expressive ? $this->user : $expressive);

        $this->assertEquals($expected, $received->count());
        $this->assertNotEquals($discussion->expressions->count(), $received->count());
    }

    /**
     * @test
     * @depends      it_adds_emotion
     * @dataProvider expressivesForProvider
     * @param $emotion
     * @param $expected
     */
    public function it_gets_expressive_for($emotion, $expected): void
    {
        $this->seedExpressions($this->discussion, $this->emotion, $this->user);
        $discussion = Discussion::find($this->discussion->getKey());
        $received = $this->emotionService->expressivesFor($discussion, is_object($emotion) ? $this->emotion : $emotion);

        $this->assertEquals($expected, $received->count());
        $this->assertNotEquals($discussion->expressions->count(), $received->count());
    }

    /**
     * @test
     * @depends      it_adds_emotion
     * @dataProvider countsEmotionsProviders
     * @param $emotion
     * @param $expected
     */
    public function it_counts_emotions($emotion, $expected): void
    {
        $this->seedExpressions($this->discussion, $this->emotion, $this->user);
        $discussion = Discussion::find($this->discussion->getKey());

        $discussion->unsetRelation('expressions');
        $received = $this->emotionService->countExpressions($discussion, is_object($emotion) ? $this->emotion : $emotion);

        $this->assertEquals($expected, $received);
    }

    protected function setUp(): void
    {
        parent::setUp();
        $this->emotionService = $this->app->make(EmotionService::class);
        $this->user = User::find(1);
        $this->other_user = User::find(2);
        $this->emotion = Emotion::first();
        $this->other_emotion = Emotion::all()->get(1);
        $this->discussion = Discussion::create(['name' => 'Discussion']);
    }
}
