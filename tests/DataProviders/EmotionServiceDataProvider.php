<?php

namespace Jackrobin\Emotion\Tests\DataProviders;

use Jackrobin\Emotion\Tests\Stubs\User;
use Jackrobin\Emotion\Tests\Stubs\Dummy;

trait EmotionServiceDataProvider
{
    /**
     * @return array In shape [$emotion, $expressive]
     */
    public function emotionProvider()
    {
        $emotion = new Dummy('like');
        $user = new Dummy(1);
        $expected = [
            'emotion_id' => (string) $emotion->getKey(),
            'expressive_id' => (string) $user->getKey(),
            'expressive_type' => User::class,
        ];

        return [
            // shape      [$emotion, $expressive]
            'entities' => [$emotion, $user, ['expressive_type' => Dummy::class] + $expected],
            'ids' => [$emotion->getKey(), $user->getKey(), $expected],
        ];
    }

    /**
     * @return array In shape [$emotional, $emotion, $expressive, expected decrement]
     */
    public function removeExpressionProvider()
    {
        $emotion = new Dummy('like');
        $user = new Dummy(1);

        return [
            // shape                     [emotion, expressive, expected decrement]
            'entities' => [$emotion, $user, -1],
            'entities without expressive' => [$emotion, null, -2],
            'ids' => [$emotion->getKey(), $user->getKey(), -1],
            'ids without expressive' => [$emotion->getKey(), null, -2],
        ];
    }

    /**
     * @return array In format [$emotion, expected decrement]
     */
    public function removeExpressionsProvider()
    {
        $user = new Dummy(1);

        return [
            // shape           [expressive, expected decrement]
            'entity' => [$user, -2],
            'id' => [$user->getKey(), -2],
            'without expressive' => [null, -3],
        ];
    }

    /**
     * @return array In shape [$emotion, $expressive, expected]
     */
    public function hasExpressionProvider()
    {
        $emotion = new Dummy('like');
        $user = new Dummy(1);

        return [
            // shape                   [rmoyion, expressive, expected]
            'entity' => [$emotion, $user, 1],
            'entity without expressive' => [$emotion, null, 2],
            'id' => [$emotion, $user->getKey(), 1],
            'id without expressive' => [$emotion, null, 2],
        ];
    }

    /**
     * @return array In shape [$expressive, expected]
     */
    public function emotionsForProvider()
    {
        $user = new Dummy(1);

        return [
            // shape    [expressive, expected]
            'entity' => [$user, 2],
            'id' => [$user->getKey(), 2],
        ];
    }

    /**
     * @return array In shape [$expressive, expected]
     */
    public function expressivesForProvider()
    {
        $emotion = new Dummy('like');

        return [
            // shape    [expressive, expected]
            'entity' => [$emotion, 2],
            'id' => [$emotion->getKey(), 2],
        ];
    }

    /**
     * @return array In shape [$expressive, expected]
     */
    public function countsEmotionsProviders()
    {
        $emotion = new Dummy('like');

        return [
            // shape    [expressive, expected]
            'entity' => [$emotion, 2],
            'id' => [$emotion->getKey(), 2],
            'none' => [null, 3],
        ];
    }
}
