# Data Providers

DataProviders are class wrappers for PHPUnit's data provider methods. 
Data provider methods are welcome to live in the test class they're 
providing for, but offloading them to a helper class can improve
readability of the test class.