<?php

namespace Jackrobin\Emotion\Tests\Stubs;

use Jackrobin\Emotion\Traits\Expressive;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Jackrobin\Contracts\Emotion\Traits\Expressive as ExpressiveContract;

class User extends Authenticatable implements ExpressiveContract
{
    use Expressive;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];
}
