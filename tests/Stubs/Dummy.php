<?php

namespace Jackrobin\Emotion\Tests\Stubs;

class Dummy
{
    public $id;

    public function __construct($id)
    {
        $this->id = $id;
    }

    public function getKey()
    {
        return $this->id;
    }

    public function fresh()
    {
    }
}
