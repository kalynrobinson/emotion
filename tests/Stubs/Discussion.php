<?php

namespace Jackrobin\Emotion\Tests\Stubs;

use Illuminate\Database\Eloquent\Model;
use Jackrobin\Emotion\Traits\Emotional;
use Jackrobin\Contracts\Emotion\Traits\Emotional as EmotionalContract;

class Discussion extends Model implements EmotionalContract
{
    use Emotional;

    protected $primaryKey = 'discussion_id';
    protected $guarded = [];

    /** {@inheritdoc} */
    public function getRouteKeyName(): string
    {
        return 'discussion_id';
    }
}
