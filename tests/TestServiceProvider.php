<?php

namespace Jackrobin\Emotion\Tests;

use Illuminate\Support\ServiceProvider as LaravelServiceProvider;

class TestServiceProvider extends LaravelServiceProvider
{
    /**
     * {@inheritDpc}.
     */
    protected $defer = false;

    /**
     * Bootstrap the application events.
     */
    public function boot()
    {
        $this->loadMigrationsFrom(
            realpath(__DIR__.'/../database/migrations')
        );
        $this->loadMigrationsFrom(
            realpath(__DIR__.'/database/migrations')
        );
    }
}
